package com.zaozao.multidb;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.zaozao.multidb.provider.person.DataBaseColumns;
import com.zaozao.multidb.provider.person.DataBaseCursorWrapper;
import com.zaozao.multidb.provider.person.PersonColumns;
import com.zaozao.multidb.provider.person.PersonCursorWrapper;

public class MainActivity extends ActionBarActivity {
    EditText dbName;
    TextView dbDisplayName;
    Button addDB;
    Button addPerson;
    TextView personData;
    EditText personName;

    ContentResolver mResolver;
    private String uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dbName = (EditText) findViewById(R.id.dbName);
        dbDisplayName = (TextView) findViewById(R.id.db_display_name);
        addDB = (Button) findViewById(R.id.addDb);
        addPerson = (Button) findViewById(R.id.addPerson);
        personData = (TextView) findViewById(R.id.personData);
        personName = (EditText) findViewById(R.id.personName);

        mResolver = getContentResolver();

        addDB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = dbName.getText().toString().trim();
                if (text.length() != 0) {
                    Cursor cursor = mResolver.query(DataBaseColumns.CONTENT_URI, DataBaseColumns.FULL_PROJECTION, DataBaseColumns.DB_UID + " = ?", new String[]{text}, null, null);
                    int count = cursor.getCount();
                    if (cursor == null || cursor.getCount() == 0) {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(DataBaseColumns.DB_UID, "" + text);
                        contentValues.put(DataBaseColumns.DB_NAME, "" + "uid" + text);
                        mResolver.insert(DataBaseColumns.CONTENT_URI, contentValues);

                        dbDisplayName.setText( "uid" + text);
                    } else {
                        cursor.moveToFirst();
                        DataBaseCursorWrapper dataBaseCursorWrapper = new DataBaseCursorWrapper(cursor);
                        dbDisplayName.setText(dataBaseCursorWrapper.getDbName());
                    }

                    uid = text;

                    cursor = mResolver.query(PersonColumns.CONTENT_URI, PersonColumns.FULL_PROJECTION, null, null, null);
                    String personStrs = "";
                    if (cursor.moveToFirst()) {
                        PersonCursorWrapper personCursorWrapper = new PersonCursorWrapper(cursor);
                        do {
                            personStrs = personStrs + "\n" + personCursorWrapper.getFirstName();
                        } while (cursor.moveToNext());
                    }
                    personData.setText(personStrs);
                    cursor.close();
                }
            }
        });

        addPerson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = personName.getText().toString().trim();
                if (text.length() != 0 && uid != null) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(PersonColumns.FIRST_NAME, text);
                    mResolver.insert(PersonColumns.CONTENT_URI, contentValues);

                    personData.setText(personData.getText() + "\n" + text);
                }
            }
        });
    }
}
