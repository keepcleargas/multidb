/*
 *     ______     ______     ______     ______     ______     ______
 *    /\___  \   /\  __ \   /\  __ \   /\___  \   /\  __ \   /\  __ \
 *    \/_/  /__  \ \  __ \  \ \ \/\ \  \/_/  /__  \ \  __ \  \ \ \/\ \
 *      /\_____\  \ \_\ \_\  \ \_____\   /\_____\  \ \_\ \_\  \ \_____\
 *     \/_____/   \/_/\/_/   \/_____/   \/_____/   \/_/\/_/   \/_____/
 *   website: see <http://www.51zaozao.com/>
 */
package com.zaozao.multidb.provider.person;

import java.util.Date;

import com.zaozao.multidb.provider.base.AbstractSelection;

/**
 * Selection for the {@code person} table.
 */
public class PersonSelection extends AbstractSelection<PersonSelection> {
    public PersonSelection id(long... value) {
        addEquals(PersonColumns._ID, toObjectArray(value));
        return this;
    }

    public PersonSelection firstName(String... value) {
        addEquals(PersonColumns.FIRST_NAME, value);
        return this;
    }
    
    public PersonSelection firstNameNot(String... value) {
        addNotEquals(PersonColumns.FIRST_NAME, value);
        return this;
    }


    public PersonSelection lastName(String... value) {
        addEquals(PersonColumns.LAST_NAME, value);
        return this;
    }
    
    public PersonSelection lastNameNot(String... value) {
        addNotEquals(PersonColumns.LAST_NAME, value);
        return this;
    }


    public PersonSelection age(int... value) {
        addEquals(PersonColumns.AGE, toObjectArray(value));
        return this;
    }
    
    public PersonSelection ageNot(int... value) {
        addNotEquals(PersonColumns.AGE, toObjectArray(value));
        return this;
    }

    public PersonSelection ageGt(int value) {
        addGreaterThan(PersonColumns.AGE, value);
        return this;
    }

    public PersonSelection ageGtEq(int value) {
        addGreaterThanOrEquals(PersonColumns.AGE, value);
        return this;
    }

    public PersonSelection ageLt(int value) {
        addLessThan(PersonColumns.AGE, value);
        return this;
    }

    public PersonSelection ageLtEq(int value) {
        addLessThanOrEquals(PersonColumns.AGE, value);
        return this;
    }
}
