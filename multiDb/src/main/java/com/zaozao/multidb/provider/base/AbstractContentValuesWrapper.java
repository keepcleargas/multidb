/*
 *     ______     ______     ______     ______     ______     ______
 *    /\___  \   /\  __ \   /\  __ \   /\___  \   /\  __ \   /\  __ \
 *    \/_/  /__  \ \  __ \  \ \ \/\ \  \/_/  /__  \ \  __ \  \ \ \/\ \
 *      /\_____\  \ \_\ \_\  \ \_____\   /\_____\  \ \_\ \_\  \ \_____\
 *     \/_____/   \/_/\/_/   \/_____/   \/_____/   \/_/\/_/   \/_____/
 *   website: see <http://www.51zaozao.com/>
 */
package com.zaozao.multidb.provider.base;

import android.content.ContentValues;

public abstract class AbstractContentValuesWrapper {
    protected ContentValues mContentValues = new ContentValues();

    public ContentValues getContentValues() {
        return mContentValues;
    }
}