package com.zaozao.multidb.provider.person;

import android.net.Uri;
import android.provider.BaseColumns;

import com.zaozao.multidb.provider.MultiDbProvider;

public interface DataBaseColumns extends BaseColumns {
    String TABLE_NAME = "multidb";
    Uri CONTENT_URI = Uri.parse(MultiDbProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    String _ID = BaseColumns._ID;
    String DB_UID = "db_uid";
    String DB_NAME = "db_name";
    String DB_PATH = "db_path";

    String DEFAULT_ORDER = _ID;

    // @formatter:off
    String[] FULL_PROJECTION = new String[]{
            _ID,
            DB_UID,
            DB_NAME,
            DB_PATH
    };
}
