package com.zaozao.multidb.provider;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.zaozao.multidb.BuildConfig;
import com.zaozao.multidb.provider.person.DataBaseColumns;

/**
 * Created by kison.chen on 14-1-8.
 */
public class CommonSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = CommonSQLiteOpenHelper.class.getSimpleName();

    public static final String DATABASE_NAME = "multidb.db";
    private static final int DATABASE_VERSION = 1;

    // @formatter:off
    private static final String SQL_CREATE_TABLE_PERSON = "CREATE TABLE IF NOT EXISTS "
            + DataBaseColumns.TABLE_NAME + " ( "
            + DataBaseColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + DataBaseColumns.DB_UID + " INTEGER, "
            + DataBaseColumns.DB_NAME + " TEXT, "
            + DataBaseColumns.DB_PATH + " TEXT "
            + " );";

    // @formatter:on

    public CommonSQLiteOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public CommonSQLiteOpenHelper(Context context, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate");
        db.execSQL(SQL_CREATE_TABLE_PERSON);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);
    }
}
