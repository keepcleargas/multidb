/*
 *     ______     ______     ______     ______     ______     ______
 *    /\___  \   /\  __ \   /\  __ \   /\___  \   /\  __ \   /\  __ \
 *    \/_/  /__  \ \  __ \  \ \ \/\ \  \/_/  /__  \ \  __ \  \ \ \/\ \
 *      /\_____\  \ \_\ \_\  \ \_____\   /\_____\  \ \_\ \_\  \ \_____\
 *     \/_____/   \/_/\/_/   \/_____/   \/_____/   \/_/\/_/   \/_____/
 *   website: see <http://www.51zaozao.com/>
 */
package com.zaozao.multidb.provider.person;

import android.net.Uri;
import android.provider.BaseColumns;

import com.zaozao.multidb.provider.MultiDbProvider;

/**
 * Columns for the {@code person} table.
 */
public interface PersonColumns extends BaseColumns {
    String TABLE_NAME = "person";
    Uri CONTENT_URI = Uri.parse(MultiDbProvider.CONTENT_URI_BASE + "/" + TABLE_NAME);

    String _ID = BaseColumns._ID;
    String FIRST_NAME = "first_name";
    String LAST_NAME = "last_name";
    String AGE = "age";

    String DEFAULT_ORDER = _ID;

	// @formatter:off
    String[] FULL_PROJECTION = new String[] {
            _ID,
            FIRST_NAME,
            LAST_NAME,
            AGE
    };
    // @formatter:on
}