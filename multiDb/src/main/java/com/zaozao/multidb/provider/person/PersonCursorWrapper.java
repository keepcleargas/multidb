/*
 *     ______     ______     ______     ______     ______     ______
 *    /\___  \   /\  __ \   /\  __ \   /\___  \   /\  __ \   /\  __ \
 *    \/_/  /__  \ \  __ \  \ \ \/\ \  \/_/  /__  \ \  __ \  \ \ \/\ \
 *      /\_____\  \ \_\ \_\  \ \_____\   /\_____\  \ \_\ \_\  \ \_____\
 *     \/_____/   \/_/\/_/   \/_____/   \/_____/   \/_/\/_/   \/_____/
 *   website: see <http://www.51zaozao.com/>
 */
package com.zaozao.multidb.provider.person;

import android.database.Cursor;

import com.zaozao.multidb.provider.base.AbstractCursorWrapper;

/**
 * Cursor wrapper for the {@code person} table.
 */
public class PersonCursorWrapper extends AbstractCursorWrapper {
    public PersonCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code first_name} value.
     * Can be {@code null}.
     */
    public String getFirstName() {
        Integer index = getCachedColumnIndexOrThrow(PersonColumns.FIRST_NAME);
        return getString(index);
    }

    /**
     * Get the {@code last_name} value.
     * Can be {@code null}.
     */
    public String getLastName() {
        Integer index = getCachedColumnIndexOrThrow(PersonColumns.LAST_NAME);
        return getString(index);
    }

    /**
     * Get the {@code age} value.
     */
    public int getAge() {
        return getIntegerOrNull(PersonColumns.AGE);
    }
}
