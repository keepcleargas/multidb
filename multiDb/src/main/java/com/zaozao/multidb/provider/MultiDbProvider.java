/*
 *     ______     ______     ______     ______     ______     ______
 *    /\___  \   /\  __ \   /\  __ \   /\___  \   /\  __ \   /\  __ \
 *    \/_/  /__  \ \  __ \  \ \ \/\ \  \/_/  /__  \ \  __ \  \ \ \/\ \
 *      /\_____\  \ \_\ \_\  \ \_____\   /\_____\  \ \_\ \_\  \ \_____\
 *     \/_____/   \/_/\/_/   \/_____/   \/_____/   \/_/\/_/   \/_____/
 *   website: see <http://www.51zaozao.com/>
 */
package com.zaozao.multidb.provider;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import com.zaozao.multidb.BuildConfig;
import com.zaozao.multidb.provider.person.DataBaseColumns;
import com.zaozao.multidb.provider.person.PersonColumns;

import java.util.ArrayList;
import java.util.Arrays;

public class MultiDbProvider extends ContentProvider {
    private static final String TAG = MultiDbProvider.class.getSimpleName();

    private static final String TYPE_CURSOR_ITEM = "vnd.android.cursor.item/";
    private static final String TYPE_CURSOR_DIR = "vnd.android.cursor.dir/";

    public static final String AUTHORITY = "com.zaozao.multidb.provider";
    public static final String CONTENT_URI_BASE = "content://" + AUTHORITY;

    public static final String QUERY_NOTIFY = "QUERY_NOTIFY";
    public static final String QUERY_GROUP_BY = "QUERY_GROUP_BY";

    private static final int URI_TYPE_DB = 0;
    private static final int URI_TYPE_DB_ID = 1;

    //约定 大于 100 是 用户私有db
    private static final int URI_TYPE_PERSON = 101;
    private static final int URI_TYPE_PERSON_ID = 102;


    private static final UriMatcher URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);

    static {
        URI_MATCHER.addURI(AUTHORITY, DataBaseColumns.TABLE_NAME, URI_TYPE_DB);
        URI_MATCHER.addURI(AUTHORITY, DataBaseColumns.TABLE_NAME + "/#", URI_TYPE_DB_ID);
        URI_MATCHER.addURI(AUTHORITY, PersonColumns.TABLE_NAME, URI_TYPE_PERSON);
        URI_MATCHER.addURI(AUTHORITY, PersonColumns.TABLE_NAME + "/#", URI_TYPE_PERSON_ID);
    }

    private MultiDbSQLiteOpenHelper mMultiDbSQLiteOpenHelper;
    private CommonSQLiteOpenHelper mCommonSQLiteOpenHelper;

    @Override
    public boolean onCreate() {
        mCommonSQLiteOpenHelper = new CommonSQLiteOpenHelper(getContext());
        return true;
    }

    @Override
    public String getType(Uri uri) {
        final int match = URI_MATCHER.match(uri);
        switch (match) {
            case URI_TYPE_DB:
                return TYPE_CURSOR_DIR + DataBaseColumns.TABLE_NAME;
            case URI_TYPE_PERSON:
                return TYPE_CURSOR_DIR + PersonColumns.TABLE_NAME;
            case URI_TYPE_PERSON_ID:
                return TYPE_CURSOR_ITEM + PersonColumns.TABLE_NAME;
        }
        return null;
    }

    public int getTableId(final Uri uri) {
        if (uri == null)
            return -1;
        return URI_MATCHER.match(uri);
    }

    @Override
    public Uri insert(Uri uri, ContentValues values) {
        if (BuildConfig.DEBUG) Log.d(TAG, "insert uri=" + uri + " values=" + values);
        final String table = uri.getLastPathSegment();
        int tableId = getTableId(uri);
        long rowId;
        if (tableId < 100) {
            rowId = mCommonSQLiteOpenHelper.getWritableDatabase().insert(table, null, values);
            if (tableId == 0 && rowId != -1) {
                mMultiDbSQLiteOpenHelper = new MultiDbSQLiteOpenHelper(getContext(), "uid" + values.get("uid"));
            }
        } else {
            rowId = mMultiDbSQLiteOpenHelper.getWritableDatabase().insert(table, null, values);
        }
        String notify;
        if (rowId != -1 && ((notify = uri.getQueryParameter(QUERY_NOTIFY)) == null || "true".equals(notify))) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return uri.buildUpon().appendEncodedPath(String.valueOf(rowId)).build();
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "bulkInsert uri=" + uri + " values.length=" + values.length);
        final String table = uri.getLastPathSegment();
        final SQLiteDatabase db = mMultiDbSQLiteOpenHelper.getWritableDatabase();
        int res = 0;
        db.beginTransaction();
        try {
            for (final ContentValues v : values) {
                final long id = db.insert(table, null, v);
                db.yieldIfContendedSafely();
                if (id != -1) {
                    res++;
                }
            }
            db.setTransactionSuccessful();
        } finally {
            db.endTransaction();
        }
        String notify;
        if (res != 0 && ((notify = uri.getQueryParameter(QUERY_NOTIFY)) == null || "true".equals(notify))) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return res;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "update uri=" + uri + " values=" + values + " selection=" + selection + " selectionArgs=" + Arrays.toString(selectionArgs));
        final QueryParams queryParams = getQueryParams(uri, selection);
        final int res = mMultiDbSQLiteOpenHelper.getWritableDatabase().update(queryParams.table, values, queryParams.selection, selectionArgs);
        String notify;
        if (res != 0 && ((notify = uri.getQueryParameter(QUERY_NOTIFY)) == null || "true".equals(notify))) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return res;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "delete uri=" + uri + " selection=" + selection + " selectionArgs=" + Arrays.toString(selectionArgs));
        final QueryParams queryParams = getQueryParams(uri, selection);
        final int res = mMultiDbSQLiteOpenHelper.getWritableDatabase().delete(queryParams.table, queryParams.selection, selectionArgs);
        String notify;
        if (res != 0 && ((notify = uri.getQueryParameter(QUERY_NOTIFY)) == null || "true".equals(notify))) {
            getContext().getContentResolver().notifyChange(uri, null);
        }
        return res;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        final String groupBy = uri.getQueryParameter(QUERY_GROUP_BY);
        if (BuildConfig.DEBUG)
            Log.d(TAG, "query uri=" + uri + " selection=" + selection + " selectionArgs=" + Arrays.toString(selectionArgs) + " sortOrder=" + sortOrder
                    + " groupBy=" + groupBy);
        final QueryParams queryParams = getQueryParams(uri, selection);
        Cursor res;
        int tableId = getTableId(uri);
        if (tableId < 100) {
            res = mCommonSQLiteOpenHelper.getReadableDatabase().query(queryParams.table, projection, queryParams.selection, selectionArgs, groupBy,
                    null, sortOrder == null ? queryParams.orderBy : sortOrder);
            if (tableId == 0 && res != null) {
                //is db query 约定 uid.
                mMultiDbSQLiteOpenHelper = new MultiDbSQLiteOpenHelper(getContext(), "uid" + selectionArgs[0]);
            }
        } else {
            res = mMultiDbSQLiteOpenHelper.getReadableDatabase().query(queryParams.table, projection, queryParams.selection, selectionArgs, groupBy,
                    null, sortOrder == null ? queryParams.orderBy : sortOrder);
        }

        res.setNotificationUri(getContext().getContentResolver(), uri);
        return res;
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList<ContentProviderOperation> operations) throws OperationApplicationException {
        SQLiteDatabase db = mMultiDbSQLiteOpenHelper.getWritableDatabase();
        db.beginTransaction();
        try {
            int numOperations = operations.size();
            ContentProviderResult[] results = new ContentProviderResult[numOperations];
            int i = 0;
            for (ContentProviderOperation operation : operations) {
                results[i] = operation.apply(this, results, i);
                if (operation.isYieldAllowed()) {
                    db.yieldIfContendedSafely();
                }
                i++;
            }
            db.setTransactionSuccessful();
            return results;
        } finally {
            db.endTransaction();
        }
    }

    private static class QueryParams {
        public String table;
        public String selection;
        public String orderBy;
    }

    private QueryParams getQueryParams(Uri uri, String selection) {
        QueryParams res = new QueryParams();
        String id = null;
        int matchedId = URI_MATCHER.match(uri);
        switch (matchedId) {
            case URI_TYPE_PERSON:
            case URI_TYPE_PERSON_ID:
                res.table = PersonColumns.TABLE_NAME;
                res.orderBy = PersonColumns.DEFAULT_ORDER;
                break;
            case URI_TYPE_DB:
                res.table = DataBaseColumns.TABLE_NAME;
                res.orderBy = DataBaseColumns.DEFAULT_ORDER;
                break;
            default:
                throw new IllegalArgumentException("The uri '" + uri + "' is not supported by this ContentProvider");
        }

        switch (matchedId) {
            case URI_TYPE_PERSON_ID:
                id = uri.getLastPathSegment();
        }
        if (id != null) {
            if (selection != null) {
                res.selection = BaseColumns._ID + "=" + id + " and (" + selection + ")";
            } else {
                res.selection = BaseColumns._ID + "=" + id;
            }
        } else {
            res.selection = selection;
        }
        return res;
    }

    public MultiDbSQLiteOpenHelper getmMultiDbSQLiteOpenHelper() {
        return mMultiDbSQLiteOpenHelper;
    }

    public void setmMultiDbSQLiteOpenHelper(MultiDbSQLiteOpenHelper mMultiDbSQLiteOpenHelper) {
        this.mMultiDbSQLiteOpenHelper = mMultiDbSQLiteOpenHelper;
    }

    public static Uri notify(Uri uri, boolean notify) {
        return uri.buildUpon().appendQueryParameter(QUERY_NOTIFY, String.valueOf(notify)).build();
    }

    public static Uri groupBy(Uri uri, String groupBy) {
        return uri.buildUpon().appendQueryParameter(QUERY_GROUP_BY, groupBy).build();
    }
}
