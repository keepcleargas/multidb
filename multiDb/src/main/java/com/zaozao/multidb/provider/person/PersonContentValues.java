/*
 *     ______     ______     ______     ______     ______     ______
 *    /\___  \   /\  __ \   /\  __ \   /\___  \   /\  __ \   /\  __ \
 *    \/_/  /__  \ \  __ \  \ \ \/\ \  \/_/  /__  \ \  __ \  \ \ \/\ \
 *      /\_____\  \ \_\ \_\  \ \_____\   /\_____\  \ \_\ \_\  \ \_____\
 *     \/_____/   \/_/\/_/   \/_____/   \/_____/   \/_/\/_/   \/_____/
 *   website: see <http://www.51zaozao.com/>
 */
package com.zaozao.multidb.provider.person;

import com.zaozao.multidb.provider.base.AbstractContentValuesWrapper;

/**
 * Content values wrapper for the {@code person} table.
 */
public class PersonContentValues extends AbstractContentValuesWrapper {

    public PersonContentValues putFirstName(String value) {
        mContentValues.put(PersonColumns.FIRST_NAME, value);
        return this;
    }

    public PersonContentValues putFirstNameNull() {
        mContentValues.putNull(PersonColumns.FIRST_NAME);
        return this;
    }


    public PersonContentValues putLastName(String value) {
        mContentValues.put(PersonColumns.LAST_NAME, value);
        return this;
    }

    public PersonContentValues putLastNameNull() {
        mContentValues.putNull(PersonColumns.LAST_NAME);
        return this;
    }


    public PersonContentValues putAge(int value) {
        mContentValues.put(PersonColumns.AGE, value);
        return this;
    }


}
