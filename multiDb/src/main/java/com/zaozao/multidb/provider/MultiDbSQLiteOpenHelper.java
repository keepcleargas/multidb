/*
 *     ______     ______     ______     ______     ______     ______
 *    /\___  \   /\  __ \   /\  __ \   /\___  \   /\  __ \   /\  __ \
 *    \/_/  /__  \ \  __ \  \ \ \/\ \  \/_/  /__  \ \  __ \  \ \ \/\ \
 *      /\_____\  \ \_\ \_\  \ \_____\   /\_____\  \ \_\ \_\  \ \_____\
 *     \/_____/   \/_/\/_/   \/_____/   \/_____/   \/_/\/_/   \/_____/
 *   website: see <http://www.51zaozao.com/>
 */
package com.zaozao.multidb.provider;

import android.annotation.TargetApi;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.util.Log;

import com.zaozao.multidb.BuildConfig;
import com.zaozao.multidb.provider.person.PersonColumns;

public class MultiDbSQLiteOpenHelper extends SQLiteOpenHelper {
    private static final String TAG = MultiDbSQLiteOpenHelper.class.getSimpleName();
    private static final int DATABASE_VERSION = 1;

    // @formatter:off
    private static final String SQL_CREATE_TABLE_PERSON = "CREATE TABLE IF NOT EXISTS "
            + PersonColumns.TABLE_NAME + " ( "
            + PersonColumns._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
            + PersonColumns.FIRST_NAME + " TEXT, "
            + PersonColumns.LAST_NAME + " TEXT, "
            + PersonColumns.AGE + " INTEGER"
            + " );";

    // @formatter:on

    public MultiDbSQLiteOpenHelper(Context context, String dbName) {
        super(context, dbName, null, DATABASE_VERSION);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public MultiDbSQLiteOpenHelper(Context context, String dbName, SQLiteDatabase.CursorFactory factory, DatabaseErrorHandler errorHandler) {
        super(context, dbName, factory, DATABASE_VERSION, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if (BuildConfig.DEBUG) Log.d(TAG, "onCreate");
        db.execSQL(SQL_CREATE_TABLE_PERSON);
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (BuildConfig.DEBUG)
            Log.d(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion);
    }
}
