package com.zaozao.multidb.provider.person;

import android.database.Cursor;

import com.zaozao.multidb.provider.base.AbstractCursorWrapper;

public class DataBaseCursorWrapper extends AbstractCursorWrapper {
    public DataBaseCursorWrapper(Cursor cursor) {
        super(cursor);
    }

    /**
     * Get the {@code db_name} value.
     * Can be {@code null}.
     */
    public String getDbName() {
        Integer index = getCachedColumnIndexOrThrow(DataBaseColumns.DB_NAME);
        return getString(index);
    }

    /**
     * Get the {@code db_path} value.
     * Can be {@code null}.
     */
    public String getDbPath() {
        Integer index = getCachedColumnIndexOrThrow(DataBaseColumns.DB_PATH);
        return getString(index);
    }
}

